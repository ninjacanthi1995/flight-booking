###################
# BUILD FOR LOCAL DEVELOPMENT
###################

FROM node:18-alpine As development

# Create app directory
WORKDIR /app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure copying both package.json AND package-lock.json (when available).
# Copying this first prevents re-running npm install on every code change.

RUN npm install -g @nestjs/cli

COPY package*.json ./

# Install app dependencies using the `npm ci` command instead of `npm install`
RUN npm i

# RUN mkdir -p /usr/src/app/dist && chmod -R 777 /usr/src/app/dist

# Bundle app source
COPY . .
EXPOSE 8000

# Use the node user from the image (instead of the root user)
# USER node
CMD ["npm", "run", "start:dev"]


###################
# BUILD FOR PRODUCTION
###################

FROM node:18-alpine As build

WORKDIR /app

# COPY --chown=node:node package*.json ./

RUN npm install -g @nestjs/cli
COPY package*.json ./
RUN npm i


# In order to run `npm run build` we need access to the Nest CLI.
# The Nest CLI is a dev dependency,
# In the previous development stage we ran `npm ci` which installed all dependencies.
# So we can copy over the node_modules directory from the development image into this build image.
# COPY --from=development /usr/src/app/node_modules ./node_modules

COPY . .

# Run the build command which creates the production bundle
RUN npm run build
EXPOSE 8000

# Set NODE_ENV environment variable
ENV NODE_ENV production



# Running `npm ci` removes the existing node_modules directory.
# Passing in --only=production ensures that only the production dependencies are installed.
# This ensures that the node_modules directory is as optimized as possible.
# RUN npm ci --only=production && npm cache clean --force

# RUN mkdir -p /usr/src/app/dist && chmod -R 777 /usr/src/app/dist


###################
# PRODUCTION
###################

# FROM node:18-alpine As production

# # Copy the bundled code from the build stage to the production image
# COPY --from=build /usr/src/app/node_modules ./node_modules
# COPY --from=build /usr/src/app/dist ./dist
# USER node

# Start the server using the production build
CMD [ "npm", "run", "start:prod" ]
