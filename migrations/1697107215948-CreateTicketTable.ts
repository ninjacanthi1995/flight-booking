import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateTicketTable1697107215948 implements MigrationInterface {
    name = 'CreateTicketTable1697107215948'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "ticket_entity" ("id" integer GENERATED ALWAYS AS IDENTITY NOT NULL, "flight_id" character varying NOT NULL, "seat_number" character varying NOT NULL, CONSTRAINT "PK_4c23bb38e4d566808a73a5af6ec" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "ticket_entity"`);
    }

}
