import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateTicketPayloadDto } from './dto';
import { TicketService } from './ticket.service';

@Controller('ticket')
export class TicketController {
  constructor(private readonly ticketService: TicketService) {}

  @Post()
  create(@Body() payload: CreateTicketPayloadDto): string {
    return this.ticketService.create(payload);
  }
}
