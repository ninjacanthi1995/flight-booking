import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
 
@Entity()
class TicketEntity {
  @PrimaryGeneratedColumn('identity', {
    generatedIdentity: 'ALWAYS',
  })
  id: number;
 
  @Column()
  flight_id: string;
 
  @Column()
  seat_number: string;
}
 
export default TicketEntity;