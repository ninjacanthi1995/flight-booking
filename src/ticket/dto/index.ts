export class CreateTicketPayloadDto {
  flight_id: string
  seat_number: string
}